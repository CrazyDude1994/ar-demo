﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void GlassSpawner::Awake()
extern void GlassSpawner_Awake_mB522C013DBB8043E4C9FC8CCA6AB9C91D0752AF6 (void);
// 0x00000002 System.Void GlassSpawner::Update()
extern void GlassSpawner_Update_m9C4056B4706597D312ECF25F4A9E3DAD25A39917 (void);
// 0x00000003 System.Void GlassSpawner::SetAllPlanesActive(System.Boolean)
extern void GlassSpawner_SetAllPlanesActive_m05B73F0CA3A0FE8527F37EF5177A1C9F1433771A (void);
// 0x00000004 System.Void GlassSpawner::.ctor()
extern void GlassSpawner__ctor_m471AFC0D0F6F98C5D827F0975BAC9C08DDEA0E49 (void);
static Il2CppMethodPointer s_methodPointers[4] = 
{
	GlassSpawner_Awake_mB522C013DBB8043E4C9FC8CCA6AB9C91D0752AF6,
	GlassSpawner_Update_m9C4056B4706597D312ECF25F4A9E3DAD25A39917,
	GlassSpawner_SetAllPlanesActive_m05B73F0CA3A0FE8527F37EF5177A1C9F1433771A,
	GlassSpawner__ctor_m471AFC0D0F6F98C5D827F0975BAC9C08DDEA0E49,
};
static const int32_t s_InvokerIndices[4] = 
{
	2016,
	2016,
	1673,
	2016,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	4,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
